﻿namespace BenchmarksCore
{
    public class Filter
    {
        public Filter(int firmId, int contactId)
        {
            FirmId = firmId;
            ContactId = contactId;
        }

        public int FirmId { get; }
        public int ContactId { get; }
    }
}
