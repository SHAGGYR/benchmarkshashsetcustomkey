﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkTest2;

namespace BenchmarksCore
{
    public class HashSetsComparer
    {
        private const int N = 100;

        private const int FirmIdLimit = 2169666;
        private const int ContactIdLimit = 2068;

        private readonly List<Filter> _list;

        public HashSetsComparer()
        {
            _list = new List<Filter>();
            var randomizer = new Random();
            for (var i = 0; i < N; i++)
            {
                var randomFirmId = randomizer.Next(FirmIdLimit);
                var randomContactId = randomizer.Next(ContactIdLimit);
                _list.Add(new Filter(randomFirmId, randomContactId));
            }
        }

        [Benchmark]
        public void AddToCustomKeyStructHashSet()
        {
            var firmsToCheck = new HashSet<FirmAndContactPair>();
            foreach (var item in _list)
            {
                firmsToCheck.Add(new FirmAndContactPair(item.FirmId, item.ContactId));
            }
        }

        [Benchmark]
        public void AddToStringKeyHashSet()
        {
            var firmsToCheck = new HashSet<string>();
            foreach (var item in _list)
            {
                firmsToCheck.Add($"{item.FirmId}.{item.ContactId}");
            }
        }

    }
}