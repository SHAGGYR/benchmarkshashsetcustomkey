﻿using System;

namespace BenchmarkTest2

{
    public struct FirmAndContactPair : IEquatable<FirmAndContactPair>
    {
        public int FirmId { get; }
        public int ContactId { get; }

        public FirmAndContactPair(int firmId, int contactId)
        {
            FirmId = firmId;
            ContactId = contactId;
        }

        public bool Equals(FirmAndContactPair other)
        {
            return FirmId == other.FirmId && ContactId == other.ContactId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is FirmAndContactPair other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return FirmId ^ ((283 - ContactId) * 397);
            }
        }

        //all answers here
        //https://stackoverflow.com/questions/8952003/how-does-hashset-compare-elements-for-equality/8952026#8952026https://stackoverflow.com/questions/8952003/how-does-hashset-compare-elements-for-equality/8952026#8952026
        //https://stackoverflow.com/questions/102742/why-is-397-used-for-resharper-gethashcode-override
    }
}