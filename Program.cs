﻿using System;
using BenchmarkDotNet.Running;

namespace BenchmarksCore
{
    class Program
    {
        static void Main()
        {
            var summary = BenchmarkRunner.Run<HashSetsComparer>();
            Console.ReadKey(true);
        }
    }
}
